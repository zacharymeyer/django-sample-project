# django-sample-project

This is a sample Django app following the book "Django for Beginners" by William S. Vincent.

You can find the deployed web application [here](https://enigmatic-temple-71974.herokuapp.com/)

